package com.stateauto.mule.notification;

import org.mule.runtime.api.event.Event;
import org.mule.runtime.api.metadata.TypedValue;
import org.mule.runtime.api.notification.PipelineMessageNotification;
import org.mule.runtime.api.notification.PipelineMessageNotificationListener;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.mule.extension.http.api.HttpRequestAttributes;

public class CIDListener implements PipelineMessageNotificationListener<PipelineMessageNotification> {

	private static ConcurrentMap<String, String> correlationId2cid = new ConcurrentHashMap<>();
	
	@Override
	public void onNotification(PipelineMessageNotification notification) {
		if ( notification.getAction().getActionId() == PipelineMessageNotification.PROCESS_START) {
			Event event = notification.getEvent();
						String cid = "ron:" + ((String) event.getCorrelationId()).substring(0, 7);
			TypedValue<HttpRequestAttributes> req = event.getMessage().getAttributes();
			String saSourcesystemcid = req.getValue().getHeaders().get("SA-SourceSystemCID");			
			if (saSourcesystemcid != null) {
				cid += "-" + saSourcesystemcid;
			}		
			correlationId2cid.put(event.getCorrelationId(), cid);
		} else if (notification.getAction().getActionId() == PipelineMessageNotification.PROCESS_COMPLETE) {
			correlationId2cid.remove(notification.getEvent().getCorrelationId());
		}
	}
	
	public static String toCID(String correlationId) {
		return correlationId2cid.get(correlationId);
	}
}