package com.stateauto.mule.notification.filter;

import org.apache.logging.log4j.core.Filter;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.config.Node;
import org.apache.logging.log4j.core.config.plugins.*;
import org.apache.logging.log4j.util.ReadOnlyStringMap;

import com.stateauto.mule.notification.CIDListener;

@Plugin(name = "ContextDataPutCID", category = Node.CATEGORY, elementType = Filter.ELEMENT_TYPE)
public class ContextDataPutCIDFilter extends ContextDataPutValueFilter {
	
	@Override
	public Result filter(LogEvent event) {
		ReadOnlyStringMap contextData = event.getContextData();
		String correlationId = contextData.getValue("correlationId");
		if (correlationId != null) {
			putValue(contextData, "CID", CIDListener.toCID(correlationId));
		}
		return Result.NEUTRAL;
	}	

    @PluginFactory
    public static ContextDataPutCIDFilter createFilter() {
        return new ContextDataPutCIDFilter();
    }
}